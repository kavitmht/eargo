# To run the program

1. Go to this link - https://repl.it
2. Select language c++.
3. Hit Run and follow the instructions that the program asks you

# Another way is to install gcc on your system and run the programs using the commands:-

1. g++ -Wall <filename>.cc -o <filename>
2. ./<filename>