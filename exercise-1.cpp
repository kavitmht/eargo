#include <bits/stdc++.h>

using namespace std;

/*
quaters - 25
dimes - 10
nickels - 5
pennies - 1
*/
int main()
{
  int n;
  cout<<"Enter the number for which you want price change: "<<endl;
  cin>>n;
  int numberOfQuaters = n/25;
  int diffLeftAfterQuaters = n - (numberOfQuaters * 25);
  int numberOfDimes = diffLeftAfterQuaters/10;
  int diffLeftAfterDimes = diffLeftAfterQuaters - (numberOfDimes*10);
  int numberofNickels = diffLeftAfterDimes/5;
  int diffLeftAfterNickels = diffLeftAfterDimes - (numberofNickels*5);
  cout<<"Quaters: "<<numberOfQuaters<<endl;
  cout<<"Dimes: "<<numberOfDimes<<endl;
  cout<<"Nickels: "<<numberofNickels<<endl;
  cout<<"Pennies: "<<diffLeftAfterNickels<<endl;
  return 0;
}

/*
Time Complexity: O(1)
Space Complexity: O(1)
*/