#include <bits/stdc++.h>

using namespace std;

int main()
{
  int n;
  // I am getting the input so that it's a generalised algorithm for any N.
  cout<<"Enter the number of days for which Stock prices were noted down: "<<endl;
  cin>>n;
  vector<int> stocks;

  // I store the Stock prices in the array, I can reduce the space complexity by making my calculations on the fly when I take input and space complexity will be reduced from O(n) to O(1).
  for(int i=0; i<n; i++) {
    int t;
    cin>>t;
    stocks.push_back(t);
  }
  
  int minTillNow = stocks[0];
  int maxGain = INT_MIN;
  int buyDay = 0;
  int sellDay = -1;
  int ansBuyDay = 0;
  int ansSellDay = -1;
  int ansGain = INT_MIN;
  for(int i=1; i<n; i++) {
    if(stocks[i] >= minTillNow) {
      int gain = abs(stocks[i] - minTillNow);
      if(maxGain < gain) {
        maxGain = gain;
        sellDay = i;
        ansBuyDay = buyDay + 1;
        ansSellDay = sellDay + 1;
        ansGain = gain;
      }
    } else {
      minTillNow = stocks[i];
      buyDay = i;
    }
  }

  // Corner case if stock prices for all the stocks were in decreasing format, then we never Bought or Sell the Stock
  if(ansSellDay == -1) {
    cout<<"No pair of days were good for buying stocks"<<endl;
    return 0;
  }
  cout<<"Stock Bought Day: "<<ansBuyDay<<endl;
  cout<<"Selling Day: "<<ansSellDay<<endl;
  cout<<"Gain: "<<ansGain<<endl;

  return 0;
}

/*
Time Complexity is O(n)
Space Complexity is O(n)
*/